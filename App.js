import React, { useState, useEffect } from "react";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { PersistGate } from "redux-persist/integration/react";

import { store, persistor } from "./src/redux/store";
import { navigationRef } from "./src/navigation/navigationService";
import AppNavigator from "./src/navigation/rootNavigator";
import { LogBox, BackHandler, ToastAndroid } from "react-native";
import "moment/locale/id";
const moment = require("moment-timezone");
moment.locale("id");
moment().tz("Asia/Jakarta");

LogBox.ignoreLogs(["Warning: ...", "Encountered", "Virtualized", "Each"]);

const App: () => React$Node = () => {
  const [exitApp, SETexitApp] = useState(false);
  const backAction = () => {
    if (exitApp == false) {
      SETexitApp(true);
      ToastAndroid.show("Press Again For Exit App", ToastAndroid.SHORT);
    } else if (exitApp == true) {
      BackHandler.exitApp();
    }

    setTimeout(() => {
      SETexitApp(false);
    }, 1500);
    return true;
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, [exitApp]);
  return (
    <NavigationContainer ref={navigationRef}>
      <AppNavigator />
    </NavigationContainer>
  );
};

export default App;
