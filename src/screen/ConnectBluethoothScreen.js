import React, { useEffect, useState } from "react";
import RNBluetoothClassic from "react-native-bluetooth-classic";
import ConnectionScreen from "./ConnectionScreen";
import DeviceListScreen from "./DeviceListScreen";
import { Text } from "react-native";

const ConnectBluethoothScreen = ({}) => {
  const [device, setDevice] = useState(undefined);
  const [bluetoothEnabled, setBluetoothEnabled] = useState(true);

  const selectDevice = (device) => {
    setDevice(device);
  };

  useEffect(() => {
    let enabledSubscription = RNBluetoothClassic.onBluetoothEnabled((event) =>
      onStateChanged(event)
    );
    let disabledSubscription = RNBluetoothClassic.onBluetoothDisabled((event) =>
      onStateChanged(event)
    );

    checkBluetootEnabled();

    return () => {
      enabledSubscription.remove();
      disabledSubscription.remove();
    };
  }, []);

  const checkBluetootEnabled = async () => {
    try {
      let enabled = await RNBluetoothClassic.isBluetoothEnabled();
      setBluetoothEnabled(enabled);
    } catch (error) {
      setBluetoothEnabled(false);
    }
  };

  const onStateChanged = (stateChangedEvent) => {
    setDevice(stateChangedEvent.enabled ? device : undefined);
    setBluetoothEnabled(stateChangedEvent.enabled);
  };

  const onBack = () => {
    setDevice(undefined);
  };

  return (
    <>
      {!device ? (
        <DeviceListScreen
          bluetoothEnabled={bluetoothEnabled}
          selectDevice={selectDevice}
        />
      ) : (
        <ConnectionScreen device={device} onBack={onBack} />
      )}
    </>
  );
};

export default ConnectBluethoothScreen;
