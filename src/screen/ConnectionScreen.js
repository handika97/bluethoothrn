import React, { useEffect, useRef, useState } from "react";
import RNBluetoothClassic from "react-native-bluetooth-classic";
import {
  FlatList,
  View,
  StyleSheet,
  Button,
  TextInput,
  ScrollView,
  Text,
  SafeAreaView,
  Image,
  Switch,
} from "react-native";
import { Buffer } from "buffer";

const ConnectionScreen = ({ device, onBack = () => {} }) => {
  const [attribute, setAttribute] = useState({
    text: undefined,
    data: [],
    polling: false,
    connection: false,
    connectionOptions: {
      DELIMITER: "9",
    },
  });
  const [value, setValue] = useState({
    value_1: 0,
    value_2: 0,
  });
  const [relay, setRelay] = useState({
    relay_1: false,
    relay_2: false,
    relay_3: false,
    relay_4: false,
  });
  const readInterval = useRef();
  const readSubscription = useRef();
  const disconnectSubscription = useRef();

  useEffect(() => {
    setTimeout(() => connect(), 0);

    return async () => {
      if (attribute.connection) {
        try {
          await device.disconnect();
        } catch (error) {
          // Unable to disconnect from device
        }
      }

      uninitializeRead();
    };
  }, []);

  const connect = async () => {
    try {
      let connection = await device.isConnected();
      if (!connection) {
        connection = await device.connect();
      }

      setAttribute({ ...attribute, connection: connection });
      initializeRead();
    } catch (error) {}
  };

  const disconnect = async (disconnected) => {
    try {
      if (!disconnected) {
        disconnected = await device.disconnect();
      }

      setAttribute({ ...attribute, connection: !disconnected });
    } catch (error) {}

    uninitializeRead();
  };

  const initializeRead = () => {
    disconnectSubscription.current = RNBluetoothClassic.onDeviceDisconnected(
      () => disconnect(true)
    );

    if (attribute.polling) {
      readInterval.current = setInterval(() => performRead(), 5000);
    } else {
      readSubscription.current = device.onDataReceived((data) =>
        onReceivedData(data)
      );
    }
  };

  const uninitializeRead = () => {
    if (readInterval.current) {
      clearInterval(readInterval.current);
    }
    if (readSubscription.current) {
      readSubscription.current.remove();
    }
  };

  const performRead = async () => {
    try {
      let available = await device.available();

      if (available > 0) {
        for (let i = 0; i < available; i++) {
          let data = await device.read();

          onReceivedData({ data });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onReceivedData = async (event) => {
    event.timestamp = new Date();

    if (event.data) {
      let newValue = event.data.split(";");
      if (newValue.length == 2)
        setValue({
          value_1: newValue[0],
          value_2: newValue[1],
        });
    }
  };

  useEffect(() => {
    relayControl(1);
  }, [relay.relay_1]);

  useEffect(() => {
    relayControl(2);
  }, [relay.relay_2]);

  useEffect(() => {
    relayControl(3);
  }, [relay.relay_3]);
  
  useEffect(() => {
    relayControl(4);
  }, [relay.relay_4]);

  const relayControl = async (relayNumber) => {
    if (relayNumber == 1) {
      if (relay.relay_1) {
        await RNBluetoothClassic.writeToDevice(device.address, "A");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      } else {
        await RNBluetoothClassic.writeToDevice(device.address, "a");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      }
    } else if (relayNumber == 2) {
      if (relay.relay_2) {
        await RNBluetoothClassic.writeToDevice(device.address, "B");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      } else {
        await RNBluetoothClassic.writeToDevice(device.address, "b");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      }
    } else if (relayNumber == 3) {
      if (relay.relay_3) {
        await RNBluetoothClassic.writeToDevice(device.address, "C");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      } else {
        await RNBluetoothClassic.writeToDevice(device.address, "c");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      }
    } else if (relayNumber == 4) {
      if (relay.relay_4) {
        await RNBluetoothClassic.writeToDevice(device.address, "D");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      } else {
        await RNBluetoothClassic.writeToDevice(device.address, "d");
        let data = Buffer.alloc(10, 0xef);
        console.log(data);
        await device.write(data);
      }
    }
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <ScrollView>
        <View
          style={{
            backgroundColor: "#191970",
            height: 200,
            width: "100%",
            zIndex: 0,
            borderBottomLeftRadius: 170,
            borderBottomRightRadius: 170,
            padding: 30,
          }}
        >
          <Text style={{ color: "white", fontWeight: "800", fontSize: 18 }}>
            Selamat Datang
          </Text>
          <View
            style={{
              width: 70,
              height: 70,
              backgroundColor: "white",
              borderRadius: 100,
              padding: 10,
              alignSelf: "center",
              top: 40,
            }}
          >
            <Image
              source={require("../assets/images/bluetooth.png")}
              style={{ width: "100%", height: "100%" }}
            />
          </View>
        </View>
        <View style={{ padding: 10 }}>
          <Text style={{ color: "#808080", fontWeight: "800", fontSize: 13 }}>
            Status
          </Text>
          <Text
            style={{
              color: attribute.connection ? "#191970" : "red",
              fontWeight: "800",
              fontSize: 15,
            }}
          >
            {attribute.connection ? "Connect" : "Unconnect"}
          </Text>
          <Text style={{ color: "#808080", fontWeight: "800", fontSize: 13 }}>
            Name Device
          </Text>
          <Text style={{ color: "#191970", fontWeight: "800", fontSize: 15 }}>
            {device.name}
          </Text>
          <Text style={{ color: "#808080", fontWeight: "800", fontSize: 13 }}>
            Address
          </Text>
          <Text style={{ color: "#191970", fontWeight: "800", fontSize: 15 }}>
            {device.address}
          </Text>
        </View>
        <View
          style={{
            width: "100%",
            height: 3,
            backgroundColor: "#e5e4e2",
            marginVertical: 5,
          }}
        />
        <View style={{ padding: 10 }}>
          <Text style={{ color: "#808080", fontWeight: "800", fontSize: 13 }}>
            Monitoring
          </Text>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              paddingVertical: 10,
            }}
          >
            <View
              style={{
                backgroundColor: "yellow",
                elevation: 20,
                height: 100,
                width: "45%",
                justifyContent: "center",
                alignItems: "center",
                marginBottom: 5,
              }}
            >
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 20 }}
              >
                {value.value_1}
              </Text>
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 13 }}
              >
                pressureInput
              </Text>
            </View>
            <View
              style={{
                backgroundColor: "yellow",
                elevation: 20,
                height: 100,
                width: "45%",
                justifyContent: "center",
                alignItems: "center",
                marginBottom: 5,
              }}
            >
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 20 }}
              >
                {value.value_2}
              </Text>
              <Text
                style={{ color: "black", fontWeight: "bold", fontSize: 13 }}
              >
                pressureInput
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            width: "100%",
            height: 3,
            backgroundColor: "#e5e4e2",
            marginVertical: 5,
          }}
        />
        <View style={{ padding: 10 }}>
          <Text
            style={{
              color: "#808080",
              fontWeight: "800",
              fontSize: 13,
              marginBottom: 10,
            }}
          >
            Control
          </Text>
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              elevation: 3,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginVertical: 5,
                borderBottomWidth: 0.5,
                padding: 5,
              }}
            >
              <Text
                style={{ color: "#191970", fontWeight: "800", fontSize: 15 }}
              >
                Relay 1
              </Text>
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={relay.relay_1 ? "#191970" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                disabled={!attribute.connection}
                onValueChange={(e) => {
                  setRelay({ ...relay, relay_1: e });
                }}
                value={relay.relay_1}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginVertical: 5,
                borderBottomWidth: 0.5,
                padding: 5,
              }}
            >
              <Text
                style={{ color: "#191970", fontWeight: "800", fontSize: 15 }}
              >
                Relay 2
              </Text>
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={relay.relay_2 ? "#191970" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                disabled={!attribute.connection}
                onValueChange={(e) => {
                  setRelay({ ...relay, relay_2: e });
                }}
                value={relay.relay_2}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginVertical: 5,
                borderBottomWidth: 0.5,
                padding: 5,
              }}
            >
              <Text
                style={{ color: "#191970", fontWeight: "800", fontSize: 15 }}
              >
                Relay 3
              </Text>
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={relay.relay_3 ? "#191970" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                disabled={!attribute.connection}
                onValueChange={(e) => {
                  setRelay({ ...relay, relay_3: e });
                }}
                value={relay.relay_3}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginVertical: 5,
                padding: 5,
              }}
            >
              <Text
                style={{ color: "#191970", fontWeight: "800", fontSize: 15 }}
              >
                Relay 4
              </Text>
              <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={relay.relay_4 ? "#191970" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                disabled={!attribute.connection}
                onValueChange={(e) => {
                  setRelay({ ...relay, relay_4: e });
                }}
                value={relay.relay_4}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            width: "100%",
            height: 3,
            backgroundColor: "#e5e4e2",
            marginVertical: 5,
          }}
        />
        <View style={{ padding: 10 }}>
          <Button title="Back" onPress={onBack} />
        </View>
        {/* <InputArea
          text={attribute.text}
          onChangeText={(text) => this.setState({ text })}
          onSend={() => this.sendData()}
          disabled={!attribute.connection}
        />
        <FlatList
          style={styles.connectionScreenOutput}
          inverted
          ref="scannedDataList"
          data={attribute.data}
          keyExtractor={(item) => item.timestamp.toISOString()}
          renderItem={({ item }) => (
            <View
              id={item.timestamp.toISOString()}
              flexDirection={"row"}
              justifyContent={"flex-start"}
            >
              <Text>{item.timestamp.toLocaleDateString()}</Text>
              <Text>{item.type === "sent" ? " < " : " > "}</Text>
              <Text flexShrink={1}>{item.data.trim()}</Text>
            </View>
          )}
        /> */}
      </ScrollView>
    </SafeAreaView>
  );
};

const InputArea = ({ text, onChangeText, onSend, disabled }) => {
  let style = disabled ? styles.inputArea : styles.inputAreaConnected;
  return (
    <View style={style}>
      <TextInput
        style={styles.inputAreaTextInput}
        placeholder={"Command/Text"}
        value={text}
        onChangeText={onChangeText}
        autoCapitalize="none"
        autoCorrect={false}
        onSubmitEditing={onSend}
        returnKeyType="send"
        disabled={disabled}
      />
      <Button
        style={{ backgroundColor: "red", width: 30 }}
        onPress={onSend}
        disabled={disabled}
        title="Send"
      ></Button>
    </View>
  );
};

/**
 * TextInput and Button for sending
 */
const styles = StyleSheet.create({
  connectionScreenWrapper: {
    flex: 1,
  },
  connectionScreenOutput: {
    paddingHorizontal: 8,
  },
  inputArea: {
    alignContent: "stretch",
    backgroundColor: "#ccc",
    paddingHorizontal: 16,
    paddingVertical: 6,
  },
  inputAreaConnected: {
    alignContent: "stretch",
    backgroundColor: "#90EE90",
    paddingHorizontal: 16,
    paddingVertical: 6,
  },
  inputAreaTextInput: {
    height: 40,
  },
  inputAreaSendButton: {
    justifyContent: "center",
    flexShrink: 1,
  },
});

export default ConnectionScreen;
