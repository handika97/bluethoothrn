import React, { useEffect, useState } from "react";
import RNBluetoothClassic from "react-native-bluetooth-classic";
import {
  PermissionsAndroid,
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Button,
  Text,
  SafeAreaView,
  ToastAndroid,
  Image,
} from "react-native";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";

const requestAccessFineLocationPermission = async () => {
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    {
      title: "Access fine location required for discovery",
      message:
        "In order to perform discovery, you must enable/allow " +
        "fine location access.",
      buttonNeutral: "Ask Me Later",
      buttonNegative: "Cancel",
      buttonPositive: "OK",
    }
  );
  return granted === PermissionsAndroid.RESULTS.GRANTED;
};

const DeviceListScreen = ({
  bluetoothEnabled = false,
  selectDevice = () => {},
}) => {
  const [devices, setDevices] = useState([]);

  useEffect(() => {
    getBondedDevices();
  }, [bluetoothEnabled]);

  const getBondedDevices = async () => {
    setDevices([]);
    try {
      let bonded = await RNBluetoothClassic.getBondedDevices();
      setDevices(bonded);
    } catch (error) {
      setDevices([]);

      ToastAndroid.LONG(error.message);
    }
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: "white",
      }}
    >
      <View
        style={{
          backgroundColor: "#191970",
          height: 200,
          width: "100%",
          zIndex: 0,
          borderBottomLeftRadius: 170,
          borderBottomRightRadius: 170,
          padding: 30,
        }}
      >
        <Text style={{ color: "white", fontWeight: "800", fontSize: 18 }}>
          Selamat Datang
        </Text>
        <View
          style={{
            width: 70,
            height: 70,
            backgroundColor: "white",
            borderRadius: 100,
            padding: 10,
            alignSelf: "center",
            top: 40,
          }}
        >
          <Image
            source={require("../assets/images/bluetooth.png")}
            style={{ width: "100%", height: "100%" }}
          />
        </View>
      </View>
      {bluetoothEnabled ? (
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              padding: 10,
            }}
          >
            <Text style={{ color: "#191970", fontWeight: "800", fontSize: 17 }}>
              Select Device
            </Text>
            <Pressable
              onPress={() => {
                getBondedDevices();
              }}
            >
              <Text
                style={{ color: "#808080", fontWeight: "800", fontSize: 15 }}
              >
                Refresh
              </Text>
            </Pressable>
          </View>
          <FlatList
            data={devices}
            renderItem={({ item, i }) => {
              return <DeviceListItem device={item} onPress={selectDevice} />;
            }}
            keyExtractor={(item) => item.address}
          />
        </View>
      ) : (
        <View style={{ alignItems: "center", marginTop: 100 }}>
          <Image
            source={require("../assets/images/off.png")}
            style={{ width: 100, height: 100 }}
          />
          <Text
            style={{
              color: "red",
              fontWeight: "900",
              fontSize: 20,
              marginTop: 10,
            }}
          >
            Bluetooth is OFF
          </Text>
          <Text style={{ color: "black", fontWeight: "500" }}>
            Please Turn ON Your Bluetooth!
          </Text>
        </View>
      )}
    </SafeAreaView>
  );
};

const DeviceListItem = ({ device, onPress }) => {
  const getColorCode = () => {
    let makeColorCode = "0123456789ABCDEF";
    let code = "#";
    for (let count = 0; count < 6; count++) {
      code = code + makeColorCode[Math.floor(Math.random() * 16)];
    }
    return code;
  };

  return (
    <TouchableOpacity
      onPress={() => onPress(device)}
      style={{
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        paddingVertical: 8,
        marginHorizontal: 10,
        marginVertical: 3,
        elevation: 3,
        backgroundColor: "white",
        borderRadius: 5,
      }}
    >
      <View
        style={{
          width: 10,
          height: 20,
          backgroundColor: getColorCode(),
          marginRight: 10,
          borderBottomRightRadius: 5,
          borderTopRightRadius: 5,
        }}
      ></View>
      <View>
        <Text style={{ fontWeight: "bold" }}>{device.name}</Text>
        <Text style={{ color: "#a9a9a9", fontSize: 14 }}>{device.address}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default DeviceListScreen;
