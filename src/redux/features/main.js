import { createSlice } from "@reduxjs/toolkit";
import Axios from "axios";
import { BaseUrl } from "../../utilities/BaseUrl";
import { authLogOut } from "./authSlice";
// import * as https from "https";
// import reactotron from 'reactotron-react-native';
export const slice = createSlice({
  name: "main",
  initialState: {},
  reducers: {},
});

// const agent = new https.Agent({
//   rejectUnauthorized: false,
// });

export const {} = slice.actions;

export const post = (
  link,
  data,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => (dispatch, getState) => {
  const { auth } = getState();
  Axios.post(BaseUrl + link, data, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${auth.headers.token}`,
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.data?.status_code === 401) {
        dispatch(authLogOut());
      }
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const postFormData = (
  link,
  data,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => (dispatch, getState) => {
  const { auth } = getState();
  Axios.post(BaseUrl + link, data, {
    headers: {
      // "Content-Type": "application/json",
      "Content-Type": "multipart/form-data",
      enctype: "multipart/form-data",
      Accept: "*/*",
      // Accept: "application/json",
      Authorization: `Bearer ${auth.headers.token}`,
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.data?.status_code === 401) {
        dispatch(authLogOut());
      }
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const patch = (
  link,
  data,
  // ApiClient,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => (dispatch, getState) => {
  const { auth } = getState();
  Axios.patch(BaseUrl + link, data, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${auth.headers.token}`,
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.data?.status_code === 401) {
        dispatch(authLogOut());
      }
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const get = (
  link,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => (dispatch, getState) => {
  const { auth } = getState();
  Axios.get(BaseUrl + link, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${auth.headers.token}`,
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const getCustom = (
  link,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => (dispatch, getState) => {
  const { auth } = getState();
  Axios.get(link, {
    headers: {
      "x-api-key":
        "$2y$12$1bRcSSRCwLgNCt1A77slau6H1rAlZcll6Qpzq23r/4hMPcUMFKMQa",
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};

export default slice.reducer;
