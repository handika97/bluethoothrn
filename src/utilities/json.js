export const color = {
  primary: "#00919F",
  background: "#ffffff",
  buttonColor: "#d5e000",
  secondary: "#F99500",
  secondaryLight: "#f0ca31",
  ligthGrey: "#eeeeee",
  Grey: "#999999",
  dragGrey: "#777777",
  white: "white",
  error: "red",
};
