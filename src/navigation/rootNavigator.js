import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from "react-redux";
import ConnectBluethoothScreen from "../screen/ConnectBluethoothScreen";

const Stack = createStackNavigator();

function AppNavigator() {
  const [performShow, setPerformShow] = useState(true);
  useEffect(() => {
    setTimeout(() => setPerformShow(false), 5500);
  }, []);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Auth"
        component={ConnectBluethoothScreen}
        options={{
          animationEnabled: false,
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}

export default AppNavigator;
